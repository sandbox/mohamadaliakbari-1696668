Module: Commerce Pasargad
Author: Mohammad Ali Akbari http://rastasoft.ir


Description
===========
Adds a payment method to Drupal Commerce to accept credit card payments through
the Pasargad (Iranian bank) web service.

Only works with Rials (IRR) currency.


Requirements
============

* Commerce
* Commerce Payment



Installation
============

* Copy the 'commerce_pasargad' module directory in to your Drupal
  sites/all/modules directory as usual.

* Enable the module from the Modules > COMMERCE (CONTRIB) section


Setup
=====

* Go to Commerce Admin Menu > Payment Methods and enable Pasargad Bank Gateway.

* Edit Pasargad Bank Gateway and enter Merchant Code, Terminal Code and 
  Private Key.


History
=======

First beta1 release: 24/7/2012
